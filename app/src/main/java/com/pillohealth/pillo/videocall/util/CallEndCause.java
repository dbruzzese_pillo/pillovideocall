package com.pillohealth.pillo.videocall.util;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * Created by Dario Bruzzese on 20/11/2017.
 *
 * This class is used to assign to the ends of the call a specific reason.
 * Reason could be
 * - Hung Up
 */
public class CallEndCause
{
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({NO_ANSWERED, HUNG_UP})
    public @interface CallEndCauseDef {}
    public static final int NO_ANSWERED = 3;
    public static final int HUNG_UP = 5;
}
